var current = 0;
var payload = [];
var load = false;




function wait(delay) {
  return new Promise((resolve) => setTimeout(resolve, delay));
}

function fetchRetry(url, delay, tries, fetchOptions = {}) {
  console.log("fetchRetry", delay, tries)
   function onError(err) {
    triesLeft = tries - 1;
    console.log("tries left", triesLeft);
    if (triesLeft <= 0) {
      throw err;
    }
    return wait(delay).then(() => fetchRetry(url, delay, triesLeft, fetchOptions));
  }
  return fetch(url, fetchOptions).then(res => {
    if (res.status == 404) {
      return Promise.reject();
    }
    return url;
  }
  ).catch(onError);
}


async function search() {
  if (load) {
    return;
  }
  load = true;
  payload = []; 
  const notif = document.getElementById('notif');
  notif.innerText = "Résultats: ".concat(payload.length);
  
  const input = document.getElementById('box');
  const buttons = document.getElementsByClassName('button-4');
  const preview = document.getElementById('preview');
  const errors = document.getElementById('errors');
  input.setAttribute("disabled", "true");
  for (let button of buttons) {
    button.style.display = "none";
  }
  preview.style.display = "none";
  errors.style.display = "none";

  const words = input.value.replaceAll(" ", "+");
  const response = await fetch("/search/" + words, { method: 'POST' }).catch(err => {
    errors.innerHTML = err;
    errors.style.display = "block";
    input.removeAttribute("disabled");
    load = false;
  });
  await response.json().then(async p => {
    console.log(p);
    current = 0;

    for (var prev of p) {
      console.log(prev);
      fetchRetry(prev, 800, 15).then((pr) => {
        payload.push(pr);
        display(current);
      }).catch(() => console.log("failed to load image", prev))
    };

    input.removeAttribute("disabled");
    for (let button of buttons) {
      button.style.display = "block";
    }
    preview.style.display = "block";
    load = false;
  }).catch(err => {
    errors.innerHTML = err;
    errors.style.display = "block";
    input.removeAttribute("disabled");
    load = false;
  });

}

function display(idx) {
  console.log(current)
  const e = payload[idx];

  const node = document.createElement('img');
  node.setAttribute('src', e);
  node.setAttribute('width', '100%');
  node.addEventListener("click", () => open_pdf_new_tab())

  const preview = document.getElementById('preview')
  preview.innerHTML = '';
  preview.appendChild(node);

  const notif = document.getElementById('notif');
  notif.innerText = "Résultats: ".concat(current, " / ", payload.length);
}

function display_prev() {
  current = current - 1;
  if (current < 0) {
    current = payload.length - 1;
  }
  display(current);
}

function display_next() {
  current = (current + 1) % payload.length;
  display(current);
}

function open_pdf_new_tab() {
  console.log(payload[current]);
  window.open(payload[current].replace('.png', ''), '_blank').focus();
}

function init() {
  console.log("init");
  const input = document.getElementById('box');
  // Execute a function when the user presses a key on the keyboard
  input.addEventListener("focusout", (event) => {
    search();
  });
  input.addEventListener("keypress", function(event) {
    // If the user presses the "Enter" key on the keyboard
    console.log(event.key)
    if (event.key === "Enter") {
      // Cancel the default action, if needed
      event.preventDefault();
      search();
    }
  });
}



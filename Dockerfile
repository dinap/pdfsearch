FROM rust:1.73-bookworm as build

RUN apt update && apt install -y libpoppler-glib-dev && rm -rf /var/lib/apt/lists/*

# create a new empty shell project
RUN USER=root cargo new --bin pdfsearch
WORKDIR /pdfsearch

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# this build step will cache your dependencies
RUN rustup default nightly
RUN cp src/main.rs src/server.rs
RUN cp src/main.rs src/cli.rs
RUN cargo build --release
RUN rm src/*.rs

# copy your source tree
COPY ./src ./src

# build for release
RUN cargo build --release

# our final base
FROM debian:bookworm-slim

RUN apt update && apt install -y openssl libpoppler-glib-dev && rm -rf /var/lib/apt/lists/*

ENV ROCKET_ADDRESS=0.0.0.0
ENV ROCKET_PORT=8000

WORKDIR /pdfsearch

# copy the build artifact from the build stage
COPY --from=build /pdfsearch/target/release/server .
COPY ./templates ./templates
COPY ./static ./static
RUN mkdir out

# set the startup command to run your binary
CMD ["./server"]

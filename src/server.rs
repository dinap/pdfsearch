#[macro_use]
extern crate rocket;
extern crate reqwest; // 0.9.18

use dotenv::dotenv;
use log::LevelFilter;
use rocket::fs::FileServer;
use rocket::http::{ContentType, Header};
use rocket::response::status::BadRequest;
use rocket::response::Responder;
use rocket::{response, Request, Response};
use rocket_dyn_templates::{context, Template};
use std::io::Cursor;

use rocket::serde::json::Json;

mod brave;
mod model;
mod pdf;

struct Pdf {
    data: Vec<u8>,
    title: String,
}

impl<'r> Responder<'r, 'static> for Pdf {
    fn respond_to(self, _req: &'r Request<'_>) -> response::Result<'static> {
        Response::build()
            .header(ContentType::PDF)
            .header(Header::new(
                "Content-Disposition",
                format!("inline;filename={}", self.title.clone()),
            ))
            .sized_body(self.data.len(), Cursor::new(self.data))
            .ok()
    }
}


#[post("/search/<query>")]
async fn search(query: &str) -> Result<Json<Vec<String>>, BadRequest<String>> {
    log::info!("query: {}", query);
    let payload = brave::search_pdf(query).await.map_err(|e| BadRequest(format!("Failed to do search query: {}", e)))?;
    let previews = pdf::load(&payload, query).await.map_err(|e| BadRequest(format!("Failed to load pdf files: {}", e)))?;
    Ok(Json(previews))
}

#[get("/")]
async fn index() -> Template {
    Template::render("index", context! {})
}

#[rocket::main]
async fn main() {
    dotenv().ok(); 
    pretty_env_logger::formatted_builder()
        .filter_level(LevelFilter::Info)
        .parse_env("RUST_LOG")
        .init();

    log::info!("Checking env BRAVE_TOKEN...");
    let _ = std::env::var("BRAVE_TOKEN"); 
    let _ = rocket::build()
        .mount("/", routes![index, search])
        .mount(
            "/out",
            FileServer::from(concat!(env!("CARGO_MANIFEST_DIR"), "/out")),
        )
        .mount(
            "/static",
            FileServer::from(concat!(env!("CARGO_MANIFEST_DIR"), "/static")),
        )
        .attach(Template::fairing())
        .launch()
        .await;
}

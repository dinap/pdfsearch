use log::LevelFilter;
use std::env;

use crate::model::{AppError, Result};

mod brave;
mod model;
mod pdf;

fn get_words() -> Result<String> {
    let mut args: Vec<String> = env::args().collect();

    if args.len() == 1 {
        log::info!("Example usage: pdfsearch bitcoin whitepaper");
        return Err(AppError::new("Missing args"));
    }
    args.remove(0);

    Ok(args.join("+").to_lowercase())
}

async fn load_pdf(words: &str) -> Result<Vec<String>> {
    let payload = brave::search_pdf(words).await?;
    pdf::load(&payload, words).await
}

#[tokio::main]
async fn main() {
    pretty_env_logger::formatted_builder()
        .filter_level(LevelFilter::Info)
        .parse_env("RUST_LOG")
        .init();

    let result = match get_words() {
        Err(err) => Err(err),
        Ok(words) => load_pdf(words.as_str()).await
    };

    match result {
        Err(err) => {
            log::error!("{}", err)
        }
        Ok(_) => log::info!("Done")
    }
}

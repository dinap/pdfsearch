use std::{env, fs, io::{BufReader, BufWriter, Write}, path::Path};

use crate::model::*;
use std::fs::File;

fn save_payload(payload: Payload, file_name: &str) -> Result<Payload>{
    let file_name = Path::new(&file_name);
    fs::create_dir_all(file_name.parent().unwrap())?;
    let file = File::create(file_name)?;
    let mut writer = BufWriter::new(file);
    serde_json::to_writer(&mut writer, &payload)?;
    writer.flush()?;
    Ok(payload)
}

fn payload_exists(file_name: &str) -> Result<Option<Payload>> {
    if Path::exists(Path::new(file_name)) {
        log::info!("Found payload, loading from disk");
        let file = File::open(file_name)?;
        let reader = BufReader::new(file);
        let payload: Payload = serde_json::from_reader(reader)?;
        Ok(Some(payload))
    } else {
        Ok(None)
    }
}

async fn brave_search(words: &str, file_name: &str) -> Result<Payload> {
    let url = format!(
        "https://api.search.brave.com/res/v1/web/search?q={}+filetype:pdf&country=fr",
        words
    );
    log::info!("Searching on brave: {:?}", url);
    // panic!();
    let token = env::var("BRAVE_TOKEN")?;
    let client = reqwest::Client::new();
    let response = client
        .get(url)
        .header(reqwest::header::USER_AGENT, "dinap pdfsearch")
        .header("x-subscription-token", token)
        .send()
        .await?;
    let text = response.text().await?;
    log::debug!("{:?}", text);

    let payload = serde_json::from_str::<Payload>(text.as_str())
        .expect(format!("search JSON not well-formatted: {}", text).as_str());

    save_payload(payload, file_name)
}

pub async fn search_pdf(words: &str) -> Result<Payload> {
    let words = words.to_lowercase();
    let file_name = format!("out/{}/payload.json", words);
    let payload: Result<Payload> = match payload_exists(file_name.as_str())? {
        Some(payload) => Ok(payload),
        None => brave_search(words.as_str(), file_name.as_str()).await,
    };

    payload
}

use serde::{Deserialize, Serialize};
use std::{error::Error, fmt};

#[derive(Debug, Deserialize, Serialize)]
pub struct WebResult {
    pub url: String,
    pub title: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Web {
    pub results: Vec<WebResult>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Payload {
    pub web: Web,
}

#[derive(Debug)]
pub struct AppError {
    pub details: String,
}

impl AppError {
    pub fn new(msg: &str) -> Box<AppError> {
        Box::new(AppError {
            details: msg.to_string(),
        })
    }
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for AppError {
    fn description(&self) -> &str {
        &self.details
    }
}

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

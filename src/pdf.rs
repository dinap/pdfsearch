use crate::model::*;
use cairo::Context;
use cairo::Format;
use cairo::ImageSurface;
use poppler::PopplerDocument;
use poppler::PopplerPage;
use std::{
    fs::{self, File},
    io::Cursor,
    path::Path,
};

fn pdf_to_png(file_name: &str) -> Result<()> {
    let doc: PopplerDocument = PopplerDocument::new_from_file(file_name, None)?;
    let num_pages = doc.get_n_pages();
    let title = file_name;
    let version_string = doc.get_pdf_version_string();
    let permissions = doc.get_permissions();
    let page: PopplerPage = doc
        .get_page(0)
        .ok_or(AppError::new("Failed to read pdf document"))?;
    let (w, h) = page.get_size();

    log::info!(
        "Document {} has {} page(s) and is {}x{}",
        title,
        num_pages,
        w,
        h
    );
    log::info!(
        "Version: {:?}, Permissions: {:x?}",
        version_string,
        permissions
    );

    let surface = ImageSurface::create(Format::Rgb24, (w * 2f64) as i32, (h * 2f64) as i32)?;
    let ctx = Context::new(&surface)?;

    ctx.save()?;
    ctx.set_source_rgb(1.0, 1.0, 1.0);
    ctx.paint()?;
    // ctx.restore()?;
    ctx.scale(2.0, 2.0);
    page.render(&ctx);
    ctx.restore()?;
    ctx.show_page()?;

    let png_file_name = format!("{}.png", &file_name);
    let mut f: File = File::create(&png_file_name)?;
    surface.write_to_png(&mut f).expect("Unable to write PNG");
    log::info!("Wrote png file {}", png_file_name);
    Ok(())
}

async fn fetch_url(url: String, file_name: String) -> Result<()> {
    let response = reqwest::get(url.to_string()).await?;
    match response.headers().get("content-type") {
        Some(content_type) => {
            if content_type.to_str()? != "application/pdf" {
                return Err(AppError::new("contentType not pdf"));
            }
            let pdf_file_name = format!("{}.pdf", file_name);
            let path = Path::new(&pdf_file_name);
            fs::create_dir_all(path.parent().unwrap())?;
            let mut file = File::create(path)?;
            let mut content = Cursor::new(response.bytes().await?);
            std::io::copy(&mut content, &mut file)?;
            log::info!("Wrote pdf file {}", pdf_file_name);
            pdf_to_png(pdf_file_name.as_str())?;
            Ok(())
        }
        None => {
            log::info!("No content type, ignoring url {}", &url);
            Err(AppError::new("no content type, file ignored"))
        }
    }
}

pub async fn load(payload: &Payload, words: &str) -> Result<Vec<String>> {
    Ok(payload
        .web
        .results
        .iter()
        .map(|r| {
            let file_name: String = format!("out/{}/{}", words, r.title);
            let url = r.url.to_string();
            tokio::spawn(async move { 
                fetch_url(url, file_name.clone()).await 
            });
            let file_name: String = format!("out/{}/{}.pdf.png", words, r.title);
            file_name
        })
        .collect())
}
